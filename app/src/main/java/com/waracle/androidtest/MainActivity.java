/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest;

import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.waracle.androidtest.download.DataDownloaderThread;
import com.waracle.androidtest.download.DataLoaderObserver;
import com.waracle.androidtest.download.DownloadedObject;
import com.waracle.androidtest.items.list.Item;
import com.waracle.androidtest.items.detail.ItemDetailFragment;
import com.waracle.androidtest.items.list.ItemListInteractionObserver;
import com.waracle.androidtest.items.list.ItemListRecyclerViewAdapter;
import com.waracle.androidtest.items.list.ItemsListFragment;
import com.waracle.androidtest.items.bitmap.ItemBitmapCacheObserver;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ItemListInteractionObserver, ItemBitmapCacheObserver {
    private static final String TAG = ItemListRecyclerViewAdapter.class.getSimpleName();

    private static String JSON_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/" +
            "raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_view);

        prepareListViewFromRemoteJson();

    }

    private void createListFragment(ArrayList<Item> items) {

        FragmentManager fragmentManager = getFragmentManager();
        ItemsListFragment itemsListFragment = (ItemsListFragment) fragmentManager.findFragmentByTag(ItemsListFragment.ITEMS_LIST_FRAGMENT);

        if (itemsListFragment == null) {
            itemsListFragment = ItemsListFragment.newInstance(items);

            fragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, itemsListFragment, ItemsListFragment.ITEMS_LIST_FRAGMENT)
                    .commit();

        } else {

            fragmentManager
                    .beginTransaction()
                    .show(itemsListFragment)
                    .commit();
        }

        itemsListFragment.addObserver(MainActivity.this);
    }

    private void createItemDetailFragment(Item item) {

        FragmentManager fragmentManager = getFragmentManager();
        ItemDetailFragment itemDetailFragment = (ItemDetailFragment) fragmentManager.findFragmentByTag(ItemDetailFragment.ITEM_DETAIL_FRAGMENT);

        if (itemDetailFragment == null) {
            itemDetailFragment = ItemDetailFragment.newInstance(item);

            fragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, itemDetailFragment, ItemDetailFragment.ITEM_DETAIL_FRAGMENT)
                    .addToBackStack(null)
                    .commit();

        } else {

            fragmentManager
                    .beginTransaction()
                    .show(itemDetailFragment)
                    .addToBackStack(null)
                    .commit();
        }

       // itemDetailFragment.addObserver(MainActivity.this);
    }

    private void prepareListViewFromRemoteJson() {

        try {
            URL url = new URL(JSON_URL);

            DataDownloaderThread dataDownloaderThread = new DataDownloaderThread(url, new DataLoaderObserver() {
                @Override
                public void onDownloadedObjectReady(DownloadedObject downloadedObject) {
                    // Read in charset of HTTP content.
                    String charset = downloadedObject.getCharset();

                    // Convert byte array to appropriate encoded string.
                    try {
                        String jsonText = new String(downloadedObject.getData(), charset);

                        createListFragment( Item.parseJsonString( jsonText ) );

                    } catch (UnsupportedEncodingException e) {
                       // Log.e(TAG, e.toString());
                    }
                }

                @Override
                public void onDownloadingError(IOException ioEx) {
                    ioEx.printStackTrace();
                }
            });
            dataDownloaderThread.start();

        } catch (MalformedURLException e) {
           // Log.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickPathItem(Item item) {
        Toast.makeText( getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT ).show();

        createItemDetailFragment(item);
    }

    @Override
    public void onItemBitmapCacheUpdated(String stringUrl, Bitmap bitmap) {
        //TODO: stub
    }
}
