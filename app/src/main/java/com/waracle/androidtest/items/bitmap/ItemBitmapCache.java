/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.items.bitmap;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.waracle.androidtest.download.DataDownloaderThread;
import com.waracle.androidtest.download.DataLoaderObserver;
import com.waracle.androidtest.download.DownloadedObject;
import com.waracle.androidtest.items.list.Item;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 *  I can use the Universal Image Loader library
 *  instead of my packages:
 *  "com.waracle.androidtest.items.bitmap"
 *  and
 *  "com.waracle.androidtest.download"
 */
public class ItemBitmapCache implements BitmapDecodingObserver{
    final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
    final int cacheSize = maxMemory / 8;

    private final LruCache<Integer, Bitmap> memoryCache;
    private Set<ItemBitmapCacheObserver> observerList = new HashSet<>();

    public ItemBitmapCache(Set<ItemBitmapCacheObserver> observerList){
        memoryCache = new LruCache<Integer, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(Integer key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };

        this.observerList = observerList;
    }

    public void addObserver(ItemBitmapCacheObserver observer) {
        observerList.add(observer);
    }

    public void removeObserver(ItemBitmapCacheObserver observer) {
        observerList.remove(observer);
    }

    public static Integer getKey(String stringUrl) {
        return Integer.valueOf( stringUrl.hashCode() );
    }

    public void addBitmapToMemoryCache(String stringUrl, Bitmap bitmap) {
        Integer key = getKey(stringUrl);

        if (getBitmapFromMemCache( stringUrl ) == null) {

            Log.d("BITMAP_KEY_CACHE add", String.valueOf( key ));

            synchronized (memoryCache) {
                memoryCache.put(key, bitmap);

                for(ItemBitmapCacheObserver observer : observerList) {
                    observer.onItemBitmapCacheUpdated(stringUrl, bitmap);
                }
            }
        }
    }

    public Bitmap getBitmapFromMemCache(String stringUrl) {
        Bitmap bitmap = null;

        Integer key = getKey(stringUrl);
        synchronized (memoryCache) {
            bitmap = memoryCache.get( key );

            if(bitmap !=null) {
                Log.d("BITMAP_KEY_CACHE ok get", String.valueOf( key ) );
            }
        }
        return bitmap;
    }

    public void fillItemImageInViewHolder(final Item item) {
        final URL url = item.getImageUrl();

        Bitmap cachedBitmap = getBitmapFromMemCache( item.getImageStringUrl() );
        if( cachedBitmap != null ) {
            item.getViewHolder().getImage().setImageBitmap( cachedBitmap );
        } else {

            DataDownloaderThread mImageLoaderThread = new DataDownloaderThread(url, new DataLoaderObserver() {
                @Override
                public void onDownloadedObjectReady(final DownloadedObject data) {

                        new BitmapDecodingAsyncTask(ItemBitmapCache.this, item, data.getData()).execute();

                }

                @Override
                public void onDownloadingError(IOException ioEx) {

                }
            });
            mImageLoaderThread.start();
        }
    }

    @Override
    public void onDecodingComplete(Item item, Bitmap bitmap) {
        if( bitmap != null ) {//If there isn't decoding error
            addBitmapToMemoryCache(item.getImageStringUrl(), bitmap);
            item.getViewHolder().getImage().setImageBitmap(bitmap);
        }
    }
}
