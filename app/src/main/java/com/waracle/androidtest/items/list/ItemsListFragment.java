/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.items.list;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.waracle.androidtest.R;
import com.waracle.androidtest.items.bitmap.ItemBitmapCache;
import com.waracle.androidtest.items.bitmap.ItemBitmapCacheObserver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class ItemsListFragment extends Fragment {
    private static final String TAG = ItemsListFragment.class.getSimpleName();

    public static final String ITEMS_LIST_FRAGMENT = "ItemsListFragment";
    public static final String ITEMS_LIST_BUNDLE = "ITEMS_LIST_BUNDLE";

    private final Set<ItemListInteractionObserver> itemListInteractionObserverList = new HashSet<>();
    private final static Set<ItemBitmapCacheObserver> itemBitmapCacheObserverList = new HashSet<>();


    private ItemListRecyclerViewAdapter itemListRecyclerViewAdapter = null;
    private ArrayList<Item> items = new ArrayList<>();

    public ItemsListFragment() {
        super();
    }

    /**
     *  I can use the Universal Image Loader library
     *  instead of my packages:
     *  "com.waracle.androidtest.items.bitmap"
     *  and
     *  "com.waracle.androidtest.download"
     */
    private static ItemBitmapCache itemBitmapCache = null;
    public static ItemBitmapCache getBitmapCacheInstance() {

        if( itemBitmapCache == null ) {
            itemBitmapCache = new ItemBitmapCache(itemBitmapCacheObserverList);
        }
        return itemBitmapCache;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        items = getArguments().getParcelableArrayList(ITEMS_LIST_BUNDLE);

        setRetainInstance(true);
    }

    public static ItemsListFragment newInstance(ArrayList<Item> items) {
        ItemsListFragment fragment = new ItemsListFragment();

        Bundle args = new Bundle();
        args.putParcelableArrayList(ITEMS_LIST_BUNDLE, items);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_itemlist_recyclerview, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;

            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            if( itemListRecyclerViewAdapter == null ) {
                itemListRecyclerViewAdapter = new ItemListRecyclerViewAdapter(items, itemListInteractionObserverList);
            }

            recyclerView.setAdapter( itemListRecyclerViewAdapter );
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        addObserver(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        itemListInteractionObserverList.clear();
        itemBitmapCacheObserverList.clear();
    }

    public void addObserver(Object observer) {
        if( observer instanceof ItemListInteractionObserver) {
            itemListInteractionObserverList.add( (ItemListInteractionObserver)observer);
        }

        if (observer instanceof ItemBitmapCacheObserver) {
            itemBitmapCacheObserverList.add( (ItemBitmapCacheObserver)observer);
        }
    }

    public void removeObserver(Object observer) {
        if( observer instanceof ItemListInteractionObserver) {
            itemListInteractionObserverList.remove(observer);
        }

        if (observer instanceof ItemBitmapCacheObserver) {
            itemBitmapCacheObserverList.remove(observer);
        }
    }
}
