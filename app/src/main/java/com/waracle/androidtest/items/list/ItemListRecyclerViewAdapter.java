/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.items.list;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.waracle.androidtest.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Item} and makes a call to the
 * specified {@link ItemListInteractionObserver}.
 */
public class ItemListRecyclerViewAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    private static final String TAG = ItemListRecyclerViewAdapter.class.getSimpleName();
    private List<Item> itemList = null;
    private Set<ItemListInteractionObserver> observerList = null;

    public ItemListRecyclerViewAdapter(List<Item> itemList, Set<ItemListInteractionObserver> observerList) {
        this.itemList = itemList;
        this.observerList = observerList;
    }

    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_itemlist_item_layout, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {

        final Item item = (Item) getItem(position);

        item.bind(holder, position);

        /**
         *  I can use the Universal Image Loader library
         *  instead of my packages:
         *  "com.waracle.androidtest.items.bitmap"
         *  and
         *  "com.waracle.androidtest.download"
         */
        ItemsListFragment.getBitmapCacheInstance().fillItemImageInViewHolder(item);

        holder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Item item = (Item)getItem(holder.getId());

                Log.d("holder.setOnClick" , "onClick Item id: " + String.valueOf( item.getTitle() ) );
                Log.d("holder.setOnClick", "observerList.size() : " + String.valueOf(observerList.size()));
                for(ItemListInteractionObserver listener : observerList) {
                    listener.onClickPathItem( item );
                }
            }
        });
    }

    public void addItems(ArrayList<Item> stores, int index){
        int cursize = itemList.size();
        itemList.addAll(index, stores);
        if (getItemCount() != 0) {
            notifyItemRangeInserted(index, (itemList.size() - cursize));
        } else {
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void addListener( ItemListInteractionObserver itemListInteractionObserver) {
        observerList.add(itemListInteractionObserver);
    }

    public void removeListener( ItemListInteractionObserver itemListInteractionObserver) {
        observerList.remove(itemListInteractionObserver);
    }

}
