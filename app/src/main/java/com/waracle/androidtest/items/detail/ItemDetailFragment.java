/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.items.detail;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.waracle.androidtest.R;
import com.waracle.androidtest.items.list.Item;
import com.waracle.androidtest.items.list.ItemsListFragment;

import java.util.HashSet;
import java.util.Set;

public class ItemDetailFragment extends Fragment {
    public static final String ITEM_DETAIL_FRAGMENT = "ITEM_DETAIL_FRAGMENT";
    public static final String ITEM_BUNDLE = "ITEM_BUNDLE";

    private Item item = null;
    private final Set<ItemDetailInteractionObserver> observerSet = new HashSet<>();

    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        item = getArguments().getParcelable("ITEM_BUNDLE");
        setRetainInstance(true);
    }

    public static ItemDetailFragment newInstance(Item item) {
        ItemDetailFragment fragment = new ItemDetailFragment();

        Bundle args = new Bundle();
        args.putParcelable(ITEM_BUNDLE, item);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_item, container, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.itemDetailImageView);

        /**
         *  I can use the Universal Image Loader library
         *  instead of my packages:
         *  "com.waracle.androidtest.items.bitmap"
         *  and
         *  "com.waracle.androidtest.download"
         */
        imageView.setImageBitmap(
                ItemsListFragment.getBitmapCacheInstance().getBitmapFromMemCache(item.getImageStringUrl())
        );

        TextView textView = (TextView) view.findViewById(R.id.Text1);
        textView.setText(item.getTitle());

        TextView textView2 = (TextView) view.findViewById(R.id.Text2);
        textView2.setText(item.getDesc());

        TextView textView3 = (TextView) view.findViewById(R.id.Text3);
        textView3.setText(item.getImageStringUrl());

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        addObserver(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        observerSet.clear();
    }

    public void addObserver(Object observer) {
        if( observer instanceof ItemDetailInteractionObserver) {
            observerSet.add( (ItemDetailInteractionObserver)observer);
        }
    }

    public void removeObserver(Object observer) {
        if( observer instanceof ItemDetailInteractionObserver) {
            observerSet.remove(observer);
        }
    }
}
