/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.items.bitmap;

import android.graphics.Bitmap;

import com.waracle.androidtest.items.list.Item;

/**
 *  I can use the Universal Image Loader library
 *  instead of my packages:
 *  "com.waracle.androidtest.items.bitmap"
 *  and
 *  "com.waracle.androidtest.download"
 */
public interface BitmapDecodingObserver {
    void onDecodingComplete(Item item, Bitmap bitmap );
}