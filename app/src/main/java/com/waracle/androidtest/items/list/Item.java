/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.items.list;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Item implements Parcelable{
    private static final String TAG = Item.class.getSimpleName();

    private String title;
    private String desc;
    private String imageStringUrl;
    private URL imageUrl;
    private ItemViewHolder viewHolder;

    public Item(String title, String desc, String imageStringUrl) {
        this.title = title;
        this.desc = desc;
        this.imageStringUrl = imageStringUrl;
    }

    protected Item(Parcel in) {
        title = in.readString();
        desc = in.readString();
        imageStringUrl = in.readString();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(desc);
        dest.writeString(imageStringUrl);
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getImageStringUrl() {
        return imageStringUrl;
    }

    public URL getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(URL imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ItemViewHolder getViewHolder() {
        return viewHolder;
    }

    public static ArrayList<Item> parseJsonString(String jsonString) {
        Log.d(TAG, " jsonString size: " + String.valueOf( jsonString.length() ) );

        ArrayList<Item> itemList = new ArrayList<>();

        try {
            JSONArray array = new JSONArray( jsonString );

            itemList = new ArrayList<>( array.length() );

            for (int i = 0; i < array.length(); ++i) {

                JSONObject jsonObject = array.getJSONObject(i);

                String title = jsonObject.getString("title");
                String desc = jsonObject.getString("desc");
                String urlString = jsonObject.getString("image");

                Item item = new Item(title, desc, urlString);
                item.setImageUrl( new URL(urlString) );

                itemList.add( item );
            }

        } catch (MalformedURLException | JSONException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }

        return itemList;
    }

    public void bind(ItemViewHolder holder, int position) {
        viewHolder = holder;
        viewHolder.setId( position );
        viewHolder.getTitle().setText( getTitle() );
        viewHolder.getDesc().setText( getDesc() );
    }

}
