/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.items.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.waracle.androidtest.R;

public class ItemViewHolder extends RecyclerView.ViewHolder  {
    private int id = 0;
    private final View bindedView;
    private final TextView title;
    private final TextView desc;
    private final ImageView image;

    public ItemViewHolder(View view) {
        super(view);
        bindedView = view;
        title = (TextView) getView().findViewById(R.id.title);
        desc = (TextView) getView().findViewById(R.id.desc);
        image = (ImageView) getView().findViewById(R.id.image);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + getTitle().getText() + "'";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public View getView() {
        return bindedView;
    }

    public TextView getTitle() {
        return title;
    }

    public TextView getDesc() {
        return desc;
    }

    public ImageView getImage() {
        return image;
    }
}
