/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.items.bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.waracle.androidtest.items.list.Item;
/**
 *  I can use the Universal Image Loader library
 *  instead of my packages:
 *  "com.waracle.androidtest.items.bitmap"
 *  and
 *  "com.waracle.androidtest.download"
 */
class BitmapDecodingAsyncTask extends AsyncTask<Void, Void, Bitmap> {

    private BitmapDecodingObserver bitmapDecodingObserver = null;
    private Item item;
    private byte[] data;

    public BitmapDecodingAsyncTask(BitmapDecodingObserver bitmapDecodingObserver, Item item, byte[] data) {
        this.bitmapDecodingObserver = bitmapDecodingObserver;
        this.item = item;
        this.data = data;
    }

    @Override
    protected Bitmap doInBackground(Void ...params) {
        final Bitmap bitmap = convertToBitmap( data );

        return bitmap;
    }

    @Override
    public void onPostExecute(Bitmap result) {
        bitmapDecodingObserver.onDecodingComplete(item, result);
    }

    private static Bitmap convertToBitmap(byte[] data) {
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }
}