/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.download;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.waracle.androidtest.download.utils.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DataDownloaderThread extends Thread {

    private static final String TAG = DataDownloaderThread.class.getSimpleName();

    public static final int CONNECTION_CODE_301_MOVED_PERMANENTLY = 301;

    private static final int DATA_RECEIVED = 200;
    private static final int ERROR_RECEIVED = 400;
    private static final String CONTENT_TYPE = "Content-Type";
    public static final int CONNECTION_CODE_200_OK = 200;

    private URL url = null;
    private DataLoaderObserver observer = null;
    private Object contextObject;

    public DataDownloaderThread(URL url, DataLoaderObserver observer) {
        this.url = url;
        this.observer = observer;
    }

    @Override
    public void run() {
        HttpURLConnection connection = null;
        InputStream inputStream = null;

        try {
            connection = (HttpURLConnection) url.openConnection();

            if ( connection.getResponseCode() == CONNECTION_CODE_301_MOVED_PERMANENTLY ) {
                String movedTo = connection.getHeaderField("Location");
                connection.disconnect();

                url = new URL(movedTo);

                connection = (HttpURLConnection) url.openConnection();
            }

            if ( connection.getResponseCode() == CONNECTION_CODE_200_OK ) {
                inputStream = connection.getInputStream();


                byte[] data = StreamUtils.readFully(inputStream);

                int length = connection.getContentLength();
                Log.d(TAG, "length: " + String.valueOf(length));

                String charset = parseCharset(connection.getRequestProperty(CONTENT_TYPE));
                Log.d(TAG, "charset: " + charset);

                handler.obtainMessage(DATA_RECEIVED, new DownloadedObject(contextObject, data, charset)).sendToTarget();
            }

        } catch (IOException ioEX) {
            Log.e(TAG, ioEX.toString());
            handler.obtainMessage( ERROR_RECEIVED, ioEX).sendToTarget();

        } finally {
            // Close the input stream if it exists.
            StreamUtils.close(inputStream);

            // Disconnect the connection
            if( connection != null ) {
                connection.disconnect();
            }
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case DATA_RECEIVED : {
                    if( msg.obj instanceof  DownloadedObject ) {
                        observer.onDownloadedObjectReady( (DownloadedObject) msg.obj);
                    }
                    break;
                }

                case ERROR_RECEIVED : {
                    if( msg.obj instanceof  IOException ) {
                        observer.onDownloadingError((IOException) msg.obj);
                    }
                    break;
                }

            }

        }
    };

    /**
     * Returns the charset specified in the Content-Type of this header,
     * or the HTTP default (ISO-8859-1) if none can be found.
     */
    public static String parseCharset(String contentType) {
        if (contentType != null) {
            String[] params = contentType.split(",");
            for (int i = 1; i < params.length; i++) {
                String[] pair = params[i].trim().split("=");
                if (pair.length == 2) {
                    if (pair[0].equals("charset")) {
                        return pair[1];
                    }
                }
            }
        }
        return "UTF-8";
    }

    public void setContextObject(Object contextObject) {
        this.contextObject = contextObject;
    }
}
