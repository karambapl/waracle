/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.download.utils;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

public class StreamUtils {
    private static final String TAG = StreamUtils.class.getSimpleName();

    private static final int BUFFER_READER_SIZE = 1024;
    private static final int BUFFER_INPUT_STREAM_SIZE = 10 * BUFFER_READER_SIZE;

    public static byte[] readFully(InputStream stream) {

        BufferedInputStream bufferedInputStream = new BufferedInputStream(stream, BUFFER_INPUT_STREAM_SIZE);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(BUFFER_INPUT_STREAM_SIZE);

        byte[] output;

        try {

            int bytesRead = bufferedInputStream.read();

            while (bytesRead != -1) {
                outputStream.write(bytesRead);
                bytesRead = bufferedInputStream.read();
            }

        } catch (IOException IOEx) {
            IOEx.printStackTrace();

            Log.e(TAG, IOEx.toString());
        } finally {
            output = outputStream.toByteArray();

            close(bufferedInputStream);
            close(outputStream);
        }

        return output;
    }

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }
}
