/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package com.waracle.androidtest.download;


public class DownloadedObject {
    private byte[] data;
    private String charset;
    private Object contextObject;

    public DownloadedObject(Object contextObject, byte[] data, String charset) {
        this.contextObject = contextObject;
        this.data = data;
        this.charset = charset;
    }

    public Object getContextObject() {
        return contextObject;
    }

    public byte[] getData() {
        return data;
    }

    public String getCharset() {
        return charset;
    }
}
